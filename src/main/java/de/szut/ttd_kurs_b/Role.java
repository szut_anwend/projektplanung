package de.szut.ttd_kurs_b;

public class Role {
    private String description;

    public Role(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
