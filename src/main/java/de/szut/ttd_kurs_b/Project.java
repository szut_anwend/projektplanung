package de.szut.ttd_kurs_b;

import java.util.Date;

public class Project {
    private String name;
    private String desctiption;
    private Date startDate;
    private Date endDate;
    private Integer totalHours;

    public Project(String name, String desctiption, Date startDate, Date endDate, Integer totalHours) {
        this.name = name;
        this.desctiption = desctiption;
        this.startDate = startDate;
        this.endDate = endDate;
        this.totalHours = totalHours;
    }

    public String getName() {
        return name;
    }

    public String getDesctiption() {
        return desctiption;
    }

    public void setDesctiption(String desctiption) {
        this.desctiption = desctiption;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public Integer getTotalHours() {
        return totalHours;
    }
}
